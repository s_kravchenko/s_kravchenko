import org.junit.Assert;
import org.junit.Test;

public class Task01Test {

    @Test
    public void creatingUnitMatrixTest() {

        int n = 3;
        int[][] arr = new int[][]{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

        int[][] expArr = Task01.creatingUnitMatrix(n);

        Assert.assertArrayEquals(expArr, arr);
    }

    @Test
    public void creatingNullMatrixTest() {

        int n = 3;
        int[][] arr = new int[n][n];

        int[][] expArr = Task01.creatingNullMatrix(n);

        Assert.assertArrayEquals(expArr, arr);
    }

    @Test
    public void multiplyMatrixTest() {

        int[][] matrixOne = new int[][]{{3, 2, 1}, {1, 2, 3}, {2, 3, 1}};
        int[][] matrixTwo = new int[][]{{1, 2, 3}, {2, 1, 3}, {3, 2, 1}};
        int[][] resultArr = new int[][]{{10, 10, 16}, {14, 10, 12}, {11, 9, 16}};

        int[][] expArr = Task01.multiplyMatrix(matrixOne, matrixTwo);

        Assert.assertArrayEquals(expArr, resultArr);
    }

    @Test
    public void multiplyByNumberTest() {

        int number = 3;
        int[][] matrixOne = new int[][]{{3, 2, 1}, {1, 2, 3}, {2, 3, 1}};
        int[][] resultArr = new int[][]{{9, 6, 3}, {3, 6, 9}, {6, 9, 3}};

        int[][] expArr = Task01.multiplyByNumber(matrixOne, number);

        Assert.assertArrayEquals(expArr, resultArr);
    }

    @Test
    public void sumMatrixTest() {

        int[][] matrixOne = new int[][]{{3, 2, 1}, {1, 2, 3}, {2, 3, 1}};
        int[][] matrixTwo = new int[][]{{1, 2, 3}, {2, 1, 3}, {3, 2, 1}};
        int[][] resultArr = new int[][]{{4, 4, 4}, {3, 3, 6}, {5, 5, 2}};

        int[][] expArr = Task01.sumMatrix(matrixOne, matrixTwo);

        Assert.assertArrayEquals(expArr, resultArr);
    }

    @Test
    public void calculationDeterminantTest() {

        int determinant = 48;
        int[][] matrixOne = new int[][]{{3, 2, 1, 4}, {1, 2, 3, 4}, {2, 3, 1, 4}, {5, 6, 7, 8}};

        int expDeterminant = Task01.calculationDeterminant(matrixOne);

        Assert.assertEquals(expDeterminant, determinant);
    }
}
