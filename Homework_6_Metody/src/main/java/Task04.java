import java.util.Random;

/**
 * На рисунке показан пример треугольника из чисел.
 *          7
 *        3   8
 *      8   1   0
 *    2   7   4   4
 *  4   5   2   6   5
 * Написать метод:
 * ■ выводящий значения треугольника на консоль в таком
 * виде как на рисунке;
 * ■ вычисляющий наибольшую сумму чисел, через
 * которые проходит путь, начинающийся на вершине и
 * заканчивающийся где-то на основании.
 * 1. Каждый шаг может идти диагонально вниз-направо
 * или диагонально вниз-налево.
 * 2. Количество строк в треугольнике >1, но <100.
 * 3. Числа в треугольнике все целые от 0 до 99 включительно (генерируются случайным образом).
 * В примере, описанном выше, это путь 7, 3, 8, 7, 5, дающий
 * максимальную сумму 30.
 * Программа должна выводить на экран треугольник и
 * путь, который даст максимальный результат. Для текущего
 * примера он будет такой – влево, влево, вправо, влево.
 */

public class Task04 {
    public static void main(String[] args) {

        int[][] arr = generateTriangle();

//        int[][] arr = new int[][]{{7, 0, 0, 0, 0}, {3, 8, 0, 0, 0}, {8, 1, 0, 0, 0}, {2, 7, 4, 4, 0}, {4, 5, 2, 6, 5}};
//        int sum = getMaxTrace(arr,0,0);

        int sum = getMaxTrace(arr);
        printTriangle(arr);
        System.out.println();
        System.out.println("Sum = " + sum);
    }

    private static void printTriangle(int[][] array) {

        int height = array.length;
        int k;

        for (int i = 0; i < height; i++) {
            k = 0;
            for (int j = 0; j < height; j++) {
                if (i == height - 1) {
                    System.out.printf("%-3d", array[i][j]);
                } else {
                    if (j >= height - 1 - i && j <= height - 1 + i) {
                        System.out.printf("%-3d", array[i][k]);
                        k++;
                    } else {
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }

    private static int[][] generateTriangle() {

        Random rnd = new Random();
        int height = rnd.nextInt(98) + 2;

        int[][] array = new int[height][height];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j <= i; j++) {
                array[i][j] = rnd.nextInt(100);
            }
        }

        return array;
    }

    private static int getMaxTrace(int[][] array) {

        int[] previos = new int[array.length + 1];
        int[] current = new int[previos.length];

        for (int row = array.length - 1; row >= 0; row--) {
            for (int col = 0; col <= row; col++) {
                current[col] = Math.max(previos[col], previos[col + 1]) + array[row][col];
            }
            int[] tmp = previos;
            previos = current;
            current = tmp;
        }

        return previos[0];
    }

    private static int getMaxTrace(int[][] array, int row, int col) {

        int result = array[row][col];
        if (row != array.length - 1) {
            int left = getMaxTrace(array, row + 1, col);
            int right = getMaxTrace(array, row + 1, col + 1);
            result += Math.max(left, right);
        }
        return result;
    }

}
