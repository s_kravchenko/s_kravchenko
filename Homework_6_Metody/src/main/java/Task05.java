import java.util.Scanner;

/**
 * Королю нужно убить дракона, но в его казне мало средств
 * для покупки армии. Нужно создать программу, используя
 * методы, которая поможет рассчитать минимальное количество копейщиков, которое необходимо, чтобы убить
 * дракона. C клавиатуры вводятся данные:
 * ■ здоровья дракона;
 * ■ атаки дракона;
 * ■ здоровье одного копейщика;
 * ■ атака одного копейщика.
 * Защита, меткость и т. п. не учитывать. Копейщики наносят
 * удар первыми (общий нанесенный урон – это сумма атак
 * всех живых копейщиков). Если атака дракона превышает
 * значение жизни копейщика (например, у копейщика жизни – 10, а атака – 15), то умирает несколько копейщиков, а
 * оставшийся урон идет одному из копейщиков.
 * Например, жизнь дракона – 500, атака – 55, жизнь одного копейщика – 10, атака –10, а количество копейщиков при
 * данных условиях – 20.
 * <p>
 * Лог боя для данного примера должен выглядеть так:
 * Итерация 15
 * Копейщики атакуют (урон 200) – у дракона осталось 300 жизней
 * Дракон атакует – осталось 15 копейщиков, один из которых ранен
 * (осталось 5 жизней)
 * Копейщики атакуют – у дракона осталось 150 жизней
 * Дракон атакует – осталось 9 копейщиков
 * Копейщики атакуют – у дракона осталось 60 жизней
 * Дракон атакует – осталось 4 копейщика, один из которых ранен
 * (осталось 5 жизней)
 * Копейщики атакуют – у дракона осталось 20 жизней
 * Дракон атакует и побеждает
 */

public class Task05 {
    public static void main(String[] args) {

        Scanner scanner;
        int dragonHealth = 0;
        int dragonAttack = 0;
        int spearmanHealth = 0;
        int spearmanAttack = 0;
        boolean isInt;

        System.out.println("Введите здоровье дракона:");

        do {
            scanner = new Scanner(System.in);
            isInt = scanner.hasNextInt();
            if (isInt) {
                dragonHealth = scanner.nextInt();
            }
        } while (!isCorrect(dragonHealth));

        System.out.println("Введите атаку дракона:");

        do {
            scanner = new Scanner(System.in);
            isInt = scanner.hasNextInt();
            if (isInt) {
                dragonAttack = scanner.nextInt();
                if (dragonAttack < 0) {
                    System.out.println("Не верный ввод. Введите атаку дракона:");
                }
            } else {
                System.out.println("Не верные данные. Повторите попытку");
            }
        } while (dragonAttack < 0 || !isInt);

        System.out.println("Введите здоровье копейщика:");

        do {
            scanner = new Scanner(System.in);
            isInt = scanner.hasNextInt();
            if (isInt) {
                spearmanHealth = scanner.nextInt();
            }
        } while (!isCorrect(spearmanHealth));

        System.out.println("Введите атаку копейщика:");

        do {
            scanner = new Scanner(System.in);
            isInt = scanner.hasNextInt();
            if (isInt) {
                spearmanAttack = scanner.nextInt();
                if (spearmanAttack < 0) {
                    System.out.println("Не верный ввод. Введите атаку копейщика:");
                }
            } else {
                System.out.println("Не верные данные. Повторите попытку");
            }
        } while (spearmanAttack < 0 || !isInt);

        int numberOfSpearman = calculateTheNumberOfSpearman(dragonHealth, dragonAttack, spearmanHealth, spearmanAttack);

        System.out.println("Нужно " + numberOfSpearman + " копейщиков, чтобы одолеть дракона");

        System.out.println("Журнал сражения:");

        printFightLog(dragonHealth, dragonAttack, spearmanHealth, spearmanAttack, numberOfSpearman);

        scanner.close();
    }

    private static void printFightLog(int dragonHealth, int dragonAttack, int spearmanHealth, int spearmanAttack, int numberOfSpearman) {

        double remainingSpearman = numberOfSpearman;
        int squadHealth = 0;
        int damage;

        do {
            damage = (int) Math.ceil(remainingSpearman) * spearmanAttack;
            dragonHealth -= damage;

            if (dragonHealth > 0) {

                System.out.printf("Копейщики атакуют (урон %d) - у дракона осталось %d жизней \n", damage, dragonHealth);
                squadHealth = (int) (remainingSpearman * spearmanHealth - dragonAttack);
                if (squadHealth > 0) {
                    remainingSpearman = (double) squadHealth / (double) spearmanHealth;

                    if (squadHealth % spearmanHealth != 0) {
                        System.out.printf("Дракон атакует - осталось %d копейщиков, один из которых" +
                                        " ранен (осталось %d жизней)\n", (int) Math.ceil(remainingSpearman),
                                squadHealth - (int) (remainingSpearman) * spearmanHealth);
                    } else {
                        System.out.printf("Дракон атакует - осталось %d копейщиков\n", (int) remainingSpearman);
                    }
                } else {
                    System.out.print("Дракон атакует и побеждает\n");
                }
            } else {
                System.out.printf("Копейщики атакуют (урон %d) - Дракон умирает", damage);
            }
        } while (dragonHealth > 0 && squadHealth > 0);
    }

    private static int calculateTheNumberOfSpearman(int dragonHealth, int dragonAttack, int spearmanHealth, int spearmanAttack) {

        int numberOfSpearman = 1;
        double remainingSpearman;
        int squadHealth;
        int remainingDragonHealth;

        do {

            remainingSpearman = numberOfSpearman;
            remainingDragonHealth = dragonHealth;
            squadHealth = numberOfSpearman * spearmanHealth;

            while (squadHealth > 0 && remainingDragonHealth > 0) {
                remainingDragonHealth -= (int) Math.ceil(remainingSpearman) * spearmanAttack;
                if (remainingDragonHealth > 0) {
                    squadHealth = (int) (remainingSpearman * spearmanHealth - dragonAttack);
                    remainingSpearman = (double) squadHealth / (double) spearmanHealth;
                }
            }

            if (squadHealth <= 0 && remainingDragonHealth > 0) {
                numberOfSpearman++;
            }

        } while (remainingDragonHealth > 0);

        return numberOfSpearman;
    }

    private static boolean isCorrect(int isInt) {

        if (isInt <= 0) {
            System.out.println("Не верные данные. Повторите попытку");
            return false;
        }

        return true;
    }
}
