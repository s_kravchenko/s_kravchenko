import java.util.Scanner;
import java.util.Stack;

/**
 * Напишите метод, проверяющую правильность расстановки скобок в строке, введенной с клавиатуры.
 * При правильной расстановке выполняются условия:
 * ■ количество открывающих и закрывающих скобок равно;
 * ■ внутри любой пары открывающая–соответствующая
 * закрывающая скобка, скобки расставлены правильно.
 * В строке могут присутствовать как круглые, так и квадратные скобки (и др. символы). Каждой открывающей
 * скобке соответствует закрывающая того же типа (круглой –
 * круглая, квадратной – квадратная).
 * Пример неправильной расстановки: ( [ a) b]
 * Пример правильных входных данных: (a[b](f[(g)(g)]))
 * <p>
 * Программа должна вывести результат в виде сообщения,
 * примеры:
 * ■ Правильная строка
 * ■ Ошибка отсутствие (
 * ■ Ошибка отсутствие )
 * ■ Ошибка отсутствие [
 * ■ Ошибка отсутствие ]
 */

public class Task06 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the text:");
        String text = scanner.nextLine();

        theTextChecker(text);
        scanner.close();
    }

    public static void theTextChecker(String text) {

        Stack<Character> theStack = new Stack<Character>();
        int k = 0;

        for (int j = 0; j < text.length(); j++) {
            char ch = text.charAt(j);
            switch (ch) {
                case '[':  // открывающие скобки
                case '(':
                    theStack.push(ch);
                    break;

                case ']':  // закрывающие скобки
                case ')':
                    if (!theStack.isEmpty())
                    {
                        char chx = theStack.pop();
                        if ((ch == ']' && chx != '[')) {
                            System.out.println("Error: missing [");
                            k++;
                        } else if ((ch == ')' && chx != '(')) {
                            System.out.println("Error: missing (");
                            k++;
                        }
                    } else

                        if (ch == ']') {
                            System.out.println("Error: missing [");
                            k++;
                        } else {
                            System.out.println("Error: missing (");
                            k++;
                        }
                    break;
                default:
                    break;
            }
        }
        if (!theStack.isEmpty())
            System.out.println("Error: missing right delimiter");
        if (k == 0) {
            System.out.println("Text is correct");
        }

    }
}
