import java.util.Random;

/**
 * В массиве хранится n явно заданных текстовых строк. Создать метод:
 * ■ выводящий содержимое массива в строку через пробел;
 * ■ сортирующий массив в обратном порядке (без учета
 * регистра) от z до a;
 * ■ сортирующий массив по количеству слов в строке (слова
 * разделены пробелами).
 * Программа должна вывести строки в начальном и отсортированном порядке.
 * Дополнительно: 1 балл за генерацию случайных уникальных строк реализованных в виде метода.
 */

public class Task03 {
    public static void main(String[] args) {

        String[] stringArrayOne = new String[]{"C", "a", "b", "d", "ac", "av", "z", "abz", "az", "za"};
        String[] stringArrayTwo = new String[]{"This", "is", "my", "string,", "words", "which", "will", "be", "split", "to", "array"};
        String[] stringArrayThree = new String[]{"This is my", "string, words", "which", "will be", "split", "to array"};
        String[] randomString = new String[]{lineGeneration(5), lineGeneration(3), lineGeneration(10)};

        System.out.println("Массив случайных строк:");
        printStringArray(randomString);
        System.out.println();

        //Проверка глубокой сортировки массива в обратном порядке на простом массиве
        System.out.println("Начальный массив:");
        printStringArray(stringArrayOne);
        deepReverseSortStringArray(stringArrayOne);
        System.out.println("Тщательно отсортированный в обратном порядке массив:");
        printStringArray(stringArrayOne);
        System.out.println();

        //Проверка обычной сортировки массива в обратном порядке на более сложном массиве
        System.out.println("Начальный массив:");
        printStringArray(stringArrayTwo);
        reverseSortStringArray(stringArrayTwo);
        System.out.println("Отсортированный в обратном порядке массив:");
        printStringArray(stringArrayTwo);
        System.out.println();

        //Проверка глубокой сортировки массива в обратном порядке на более сложном массиве
        stringArrayTwo = new String[]{"This", "is", "my", "string,", "words", "which", "will", "be", "split", "to", "array"};
        System.out.println("Начальный массив:");
        printStringArray(stringArrayTwo);
        deepReverseSortStringArray(stringArrayTwo);
        System.out.println("Тщательно отсортированный в обратном порядке массив:");
        printStringArray(stringArrayTwo);
        System.out.println();

        //Проверка сортировки массива по колличеству слов в строке
        System.out.println("Начальный массив:");
        printStringArray(stringArrayThree);
        sortStringArrayByTheNumberOfWordsInRows(stringArrayThree);
        System.out.println("Отсортированный массив по колличеству слов в строке:");
        printStringArray(stringArrayThree);
        System.out.println();

    }

    private static void printStringArray(String[] array) {

        for (String value : array) {
            System.out.print(value + " ");
        }

        System.out.println();
    }

    private static void deepReverseSortStringArray(String[] array) {

        String temp;
        boolean changed = true;

        while (changed) {
            changed = false;

            for (int i = 0; i < array.length - 1; i++) {
                int min;
                if (array[i].length() > array[i + 1].length()) {
                    min = array[i + 1].length();
                } else {
                    min = array[i].length();
                }
                if (min > 1) {
                    for (int j = 1; j <= min - 1; j++) {
                        if ((array[i + 1].toLowerCase().charAt(0) > array[i].toLowerCase().charAt(0))) {
                            changed = true;
                            temp = array[i];
                            array[i] = array[i + 1];
                            array[i + 1] = temp;
                        } else if ((array[i + 1].toLowerCase().charAt(j - 1) == array[i].toLowerCase().charAt(j - 1)) &&
                                (array[i + 1].toLowerCase().charAt(j) > array[i].toLowerCase().charAt(j))) {
                            changed = true;
                            temp = array[i];
                            array[i] = array[i + 1];
                            array[i + 1] = temp;
                        } else {
                            break;
                        }
                    }
                } else {
                    if ((array[i + 1].toLowerCase().charAt(0) > array[i].toLowerCase().charAt(0)) ||
                            (array[i + 1].toLowerCase().charAt(0) == array[i].toLowerCase().charAt(0)) &&
                                    array[i + 1].length() > array[i].length()) {
                        changed = true;
                        temp = array[i];
                        array[i] = array[i + 1];
                        array[i + 1] = temp;
                    }
                }
            }
        }
    }

    private static void reverseSortStringArray(String[] array) {

        String temp;
        boolean changed = true;

        while (changed) {
            changed = false;

            for (int i = 0; i < array.length - 1; i++) {
                if (array[i + 1].toLowerCase().charAt(0) > array[i].toLowerCase().charAt(0)) {
                    changed = true;
                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
        }
    }

    private static void sortStringArrayByTheNumberOfWordsInRows(String[] array) {

        for (int j = array.length - 1; j > 0; j--) {
            for (int k = 0; k < j; k++) {
                if (array[k].split("[\\s]+").length > array[k + 1].split("[\\s]+").length) {
                    String temp = array[k];
                    array[k] = array[k + 1];
                    array[k + 1] = temp;
                }
            }
        }
    }

    private static String lineGeneration(int length) {

        final String symbols = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz,:!?. ";
        Random rnd = new Random();

        StringBuilder line = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            line.append(symbols.charAt(rnd.nextInt(symbols.length())));
        }
        return line.toString();
    }

}
