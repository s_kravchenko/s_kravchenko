/**
 * Написать и протестировать методы работы с квадратными матрицами (матрицы представить в виде двухмерных
 * массивов).
 * Должны присутствовать методы:
 * ■ создания единичной (диагональной) матрицы;
 * ■ создания нулевой матрицы;
 * ■ сложение матриц;
 * ■ умножения матриц;
 * ■ умножение матрицы на скаляр;
 * ■ определение детерминанта матрицы;
 * ■ вывод матрицы на консоль.
 */

public class Task01 {
    public static void main(String[] args) {

        int n = 3;
        int[][] unitMatrix = creatingUnitMatrix(n);
        int[][] nullMatrix = creatingNullMatrix(n);
        int[][] matrixOne = new int[][]{{3, 2, 1}, {1, 2, 3}, {2, 3, 1}};
        int[][] matrixTwo = new int[][]{{1, 2, 3}, {2, 1, 3}, {3, 2, 1}};
        int[][] matrixThree;

        int x = calculationDeterminant(matrixOne);
        int y = calculationDeterminant(matrixTwo);

        System.out.println("Первая матрица");
        printMatrix(matrixOne);
        System.out.println();

        System.out.println("Вторая матрица");
        printMatrix(matrixTwo);
        System.out.println();

        System.out.println("Детерминант первой матрицы = " + x);
        System.out.println("Детерминант второй матрицы = " + y);
        System.out.println();

        System.out.println("Результат суммирования матриц: ");
        matrixThree = sumMatrix(matrixOne, matrixTwo);
        printMatrix(matrixThree);
        System.out.println();

        System.out.println("Результат умножения первой матрицы на 2:");
        matrixThree = multiplyByNumber(matrixOne, 2);
        printMatrix(matrixThree);
        System.out.println();

        System.out.println("Результат перемножения матриц:");
        matrixThree = multiplyMatrix(matrixOne, matrixTwo);
        printMatrix(matrixThree);
        System.out.println();

        System.out.println("Еденичная матрица:");
        printMatrix(unitMatrix);
        System.out.println();

        System.out.println("Нулевая матрица:");
        printMatrix(nullMatrix);
        System.out.println();

        System.out.println("Умножение первой матрицы на еденичную матрицу:");
        matrixThree = multiplyMatrix(matrixOne, unitMatrix);
        printMatrix(matrixThree);
        System.out.println();

        System.out.println("Умножение первой матрицы на нулевую матрицу:");
        matrixThree = multiplyMatrix(matrixOne, nullMatrix);
        printMatrix(matrixThree);
        System.out.println();
    }

    static int[][] creatingUnitMatrix(int n) {

        int[][] arr = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    arr[i][j] = 1;
                }
            }
        }

        return arr;
    }

    private static void printMatrix(int[][] arr) {

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }

    }

    static int[][] creatingNullMatrix(int n) {

        return new int[n][n];
    }

    static int[][] multiplyMatrix(int[][] arrOne, int[][] arrTwo) {

        int[][] arr = new int[arrOne.length][arrTwo[0].length];

        for (int i = 0; i < arrOne.length; i++) {
            for (int j = 0; j < arrTwo[0].length; j++) {
                for (int k = 0; k < arrTwo.length; k++) {
                    arr[i][j] += arrOne[i][k] * arrTwo[k][j];
                }
            }
        }

        return arr;
    }

    static int[][] multiplyByNumber(int[][] arrOne, int number) {

        int[][] arr = new int[arrOne.length][arrOne[0].length];

        for (int i = 0; i < arrOne.length; i++) {
            for (int j = 0; j < arrOne[0].length; j++) {
                arr[i][j] = arrOne[i][j] * number;
            }
        }

        return arr;
    }

    static int[][] sumMatrix(int[][] arrOne, int[][] arrTwo) {

        int[][] arr = new int[arrOne.length][arrOne[0].length];

        for (int j = 0; j < arrOne.length; j++) {
            for (int k = 0; k < arrOne[0].length; k++) {
                arr[j][k] = arrOne[j][k] + arrTwo[j][k];
            }
        }

        return arr;
    }

    static int calculationDeterminant(int[][] arr) {

        if (arr.length == 1) {
            return arr[0][0];
        }

        int determinant = 0;

        if (arr.length == 2) {
            determinant = arr[0][0] * arr[1][1] - arr[0][1] * arr[1][0];
            return determinant;
        }

        for (int i = 0; i < arr[0].length; i++) {
            int[][] temp = new int[arr.length - 1][arr[0].length - 1];
            for (int j = 1; j < arr.length; j++) {
                System.arraycopy(arr[j], 0, temp[j - 1], 0, i);
                System.arraycopy(arr[j], i + 1, temp[j - 1], i, arr[0].length - i - 1);
            }
            determinant += arr[0][i] * Math.pow(-1, i) * calculationDeterminant(temp);
        }

        return determinant;
    }
}
