/**
 * Написать и протестировать перегруженный метод, выводящий на экран:
 * ■ одномерный массив типа int;
 * ■ одномерный массив типа String;
 * ■ двухмерный массив типа int;
 * ■ двухмерный массив типа float
 */

public class Task02 {
    public static void main(String[] args) {

        int[] integerArray = new int[]{2, 4, 6, 5, 7};
        String[] stringArray = new String[]{"asda", "qwes", "fdsfs", "awffe"};
        int[][] integerMatrix = new int[][]{{3, 2, 1}, {1, 2, 3}, {2, 3, 1}};
        float[][] floatMatrix = new float[][]{{(float) 54.2, (float) 54.3, (float) 87.6},
                {(float) 54.2, (float) 54.3, (float) 87.6}, {(float) 54.2, (float) 54.3, (float) 87.6}};

        System.out.println("Целочисленный массив:");
        printArray(integerArray);
        System.out.println();

        System.out.println("Строковый массив:");
        printArray(stringArray);
        System.out.println();

        System.out.println("Целочисленная матрица:");
        printArray(integerMatrix);
        System.out.println();

        System.out.println("Дробная матрица:");
        printArray(floatMatrix);
        System.out.println();
    }

    private static void printArray(int[] array) {

        for (int value : array) {
            System.out.print(value + "\t");
        }
        System.out.println();

    }

    private static void printArray(String[] array) {

        for (String value : array) {
            System.out.print(value + "\t");
        }
        System.out.println();

    }

    private static void printArray(int[][] array) {

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }

    }

    private static void printArray(float[][] array) {

        for (float[] floats : array) {
            for (float aFloat : floats) {
                System.out.print(aFloat + "\t");
            }
            System.out.println();
        }

    }

}
