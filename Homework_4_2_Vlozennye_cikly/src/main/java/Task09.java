import java.util.Scanner;

/**
 * Необходимо нарисовать ёлочку символом «звёздочка».
 * Каждый новый ярус должен быть шире предыдущего. С
 * клавиатуры вводится количество ярусов, и высота первого
 * (верхнего) яруса ёлочки (количество строк в ярусе). Ёлочка
 * должна быть симметричная.
 * Пример работы программы:
 * Введите число ярусов и высоту через пробел
 * 3 4
 *              *
 *             ***
 *            *****
 *           *******
 *              *
 *             ***
 *            *****
 *           *******
 *          *********
 *              *
 *             ***
 *            *****
 *           *******
 *          *********
 *         ***********
 */

public class Task09 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество ярусов:");
        boolean isInt = scanner.hasNextInt();
        if (isInt) {
            int numberOfTier = scanner.nextInt();
            System.out.println("Введите высоту верхнего яруса:");
            isInt = scanner.hasNextInt();
            if (isInt) {
                int height = scanner.nextInt();
                int y = numberOfTier-1;
                for (int k = 0; k < numberOfTier; k++, y--) {
                    for (int i = 0; i < height; i++) {
                        for (int j = 0; j < 2 * height - 1 + y; j++) {
                            if (i == height - 1 + y) {
                                System.out.print('*');
                            } else {
                                if (j >= height - 1 - i + y && j <= y + height - 1 + i) {
                                    System.out.print('*');
                                } else {
                                    System.out.print(' ');
                                }
                            }
                        }
                        System.out.println();
                    }
                    height += 1;
                }
            } else {
                System.out.println("Invalid value entered");
            }
        } else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
