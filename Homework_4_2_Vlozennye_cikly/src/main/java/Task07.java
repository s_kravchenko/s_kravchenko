import java.util.Scanner;

/**
 * С клавиатуры вводится целое положительное число любой разрядности. Необходимо перевернуть это число, т. е.
 * цифры должны располагаться в обратном порядке (например, вводим число 1234 – в результате будет 4321).
 * Не использовать строки и массивы.
 */

public class Task07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = 0;
        int newNumber;
        do {
            newNumber = 0;
            System.out.println("Введите число:");
            boolean isInt = scanner.hasNextInt();
            if (isInt) {
                number = scanner.nextInt();
                for (; ; ) {
                    if (number != 0) {
                        newNumber = number % 10 + newNumber * 10;
                        number /= 10;
                    } else {
                        break;
                    }
                }
                System.out.println("Число наоборот: " + newNumber);
                System.out.println("Если хотите продолжить, нажмите 1");
                isInt = scanner.hasNextInt();
                if (isInt) {
                    number = scanner.nextInt();
                } else {
                    System.out.println("Invalid value entered");
                }
            } else {
                System.out.println("Invalid value entered");
            }
        } while (number == 1);
        scanner.close();
    }
}
