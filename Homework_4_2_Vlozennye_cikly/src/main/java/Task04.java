import java.util.Scanner;

/**
 * В заданиях с 1-го по 5-е, используя операторы ветвления и
 * цикла написать программу, алгоритм которой будет выводить геометрические фигуры в консоль в виде «звёздочек»
 * (псевдографика). Грани фигур должны быть ровными, фигуры – симметричными. Высота каждой фигуры должны
 * задаваться с клавиатуры. Можно использовать вывод только
 * одной «звёздочки» в System.out.print(“*”).
 *
 * Ромб: заполненный и пустой.          *
 *                                     * *
 *                                    *   *
 *                                   *     *
 *                                  *       *
 *                                   *     *
 *                                    *   *
 *                                     * *
 *                                      *
 */

public class Task04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите сторону ромба:");
        boolean isInt = scanner.hasNextInt();
        if (isInt) {
            int n = scanner.nextInt();
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < 2 * n - 1; j++) {
                    if (j >= n - 1 - i && j <= n - 1 + i) {
                        System.out.print('*');
                    } else {
                        System.out.print(' ');
                    }
                }
                System.out.println();
            }
            for (int i = n; i < 2 * n - 1; i++) {
                for (int j = 0; j < 2 * n - 1; j++) {
                    if (j >= i - n + 1 && j <= 3 * n - 3 - i) {
                        System.out.print('*');
                    } else {
                        System.out.print(' ');
                    }
                }
                System.out.println();
            }
            System.out.println();
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < 2 * n - 1; j++) {
                    if (j == n - 1 - i || j == n - 1 + i) {
                        System.out.print('*');
                    } else {
                        System.out.print(' ');
                    }
                }
                System.out.println();
            }
            for (int i = n; i < 2 * n - 1; i++) {
                for (int j = 0; j < 2 * n - 1; j++) {
                    if (j == i - n + 1 || j == 3 * n - 3 - i) {
                        System.out.print('*');
                    } else {
                        System.out.print(' ');
                    }
                }
                System.out.println();
            }
        } else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
