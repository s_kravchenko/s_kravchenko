import java.util.Scanner;

/**
 * С клавиатуры вводится целое число любой разрядности.
 * Программа должна определить и вывести на консоль количество цифр в этом числе, а так же сумму этих чисел.
 */

public class Task08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        boolean isInt = scanner.hasNextInt();
        int count = 0;
        int sum = 0;
        if (isInt) {
            int number = scanner.nextInt();
            while (number != 0) {
                sum += Math.abs(number) % 10;
                number /= 10;
                count++;
            }
            System.out.println("Количество цифр в числе: " + count);
            System.out.println("Сумма цифр в числе = " + sum);
        } else {
            System.out.println("Invalid value entered");
        }
    }
}
