import java.util.Scanner;

/**
 * Нарисовать на экране лесенку. Количество ступенек указывает пользователь с клавиатуры.
 * Пример лесенки на 3 ступеньки:
 *          ***
 *            *
 *            ***
 *              *
 *              ***
 */

public class Task06 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество ступенек:");
        boolean isInt = scanner.hasNextInt();
        if(isInt){
            int n = scanner.nextInt();
            for (int i = 1; i <= n; i++) {

                for (int j = 1; j < i; j++) {
                    System.out.print("  ");
                }
                for (int j = 1; j <= 3; j++) {
                    System.out.print("*");
                }
                if(i < n) {
                    System.out.print("\n");
                    for (int j = 1; j < i + 1; j++) {
                        System.out.print("  ");
                    }
                    System.out.print("*");
                    System.out.println();
                }
            }
        }
        else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
