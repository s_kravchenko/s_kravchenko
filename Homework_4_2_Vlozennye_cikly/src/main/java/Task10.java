import java.util.Scanner;

/**
 * Напишите программу, которая будет считывать с консоли любое число (от 0 до 99999999) и выводить его цифры в
 * виде «звёздочек».
 */

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        boolean isInt = scanner.hasNextInt();
        if (isInt) {
            int number = scanner.nextInt();

            displayLineOne(number);
            System.out.println();
            displayLineTwo(number);
            System.out.println();
            displayLineThree(number);
            System.out.println();
            displayLineFour(number);
            System.out.println();
            displayLineFive(number);
            System.out.println();
            displayLineSix(number);
            System.out.println();
            displayLineSeven(number);
        } else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }

    public static void displayLineOne(int number) {
        String stringNumber = Integer.toString(number);
        for (int i = 0; i < stringNumber.length(); i++) {
            switch (Character.getNumericValue(stringNumber.charAt(i))) {
                case 0:
                    for (int j = 0; j < 9; j++) {
                        if (j == 2 || j == 3 || j == 4) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 1:
                    for (int j = 0; j < 5; j++) {
                        if (j == 1) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 2:
                case 3:
                case 8:
                    for (int j = 0; j < 7; j++) {
                        if (j == 1 || j == 2 || j == 3) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 4:
                    for (int j = 0; j < 8; j++) {
                        if (j == 3) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 5:
                case 6:
                case 9:
                    for (int j = 0; j < 7; j++) {
                        if (j == 1 || j == 2 || j == 3 || j == 4) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 7:
                    for (int j = 0; j < 7; j++) {
                        if (j <= 4) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
            }
        }
    }

    public static void displayLineTwo(int number) {
        String stringNumber = Integer.toString(number);
        for (int i = 0; i < stringNumber.length(); i++) {
            switch (Character.getNumericValue(stringNumber.charAt(i))) {
                case 0:
                    for (int j = 0; j < 9; j++) {
                        if (j == 1 || j == 5) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 1:
                    for (int j = 0; j < 5; j++) {
                        if (j == 0 || j == 1) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 2:
                case 8:
                case 9:
                    for (int j = 0; j < 7; j++) {
                        if (j == 0 || j == 4) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 3:
                case 7:
                    for (int j = 0; j < 7; j++) {
                        if (j == 4) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 4:
                    for (int j = 0; j < 8; j++) {
                        if (j == 2 || j == 3) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 5:
                case 6:
                    for (int j = 0; j < 7; j++) {
                        if (j == 0) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
            }
        }
    }

    public static void displayLineThree(int number) {
        String stringNumber = Integer.toString(number);
        for (int i = 0; i < stringNumber.length(); i++) {
            switch (Character.getNumericValue(stringNumber.charAt(i))) {
                case 0:
                    for (int j = 0; j < 9; j++) {
                        if (j == 0 || j == 6) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 1:
                    for (int j = 0; j < 5; j++) {
                        if (j == 1) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 2:
                    for (int j = 0; j < 7; j++) {
                        if (j == 0 || j == 3) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 3:
                    for (int j = 0; j < 7; j++) {
                        if (j == 4) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 4:
                    for (int j = 0; j < 8; j++) {
                        if (j == 1 || j == 3) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 5:
                case 6:
                    for (int j = 0; j < 7; j++) {
                        if (j == 0) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 7:
                    for (int j = 0; j < 7; j++) {
                        if (j == 3) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 8:
                case 9:
                    for (int j = 0; j < 7; j++) {
                        if (j == 0 || j == 4) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
            }
        }
    }

    public static void displayLineFour(int number) {
        String stringNumber = Integer.toString(number);
        for (int i = 0; i < stringNumber.length(); i++) {
            switch (Character.getNumericValue(stringNumber.charAt(i))) {
                case 0:
                    for (int j = 0; j < 9; j++) {
                        if (j == 0 || j == 6) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 1:
                    for (int j = 0; j < 5; j++) {
                        if (j == 1) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 2:
                case 7:
                    for (int j = 0; j < 7; j++) {
                        if (j == 2) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 3:
                case 5:
                case 6:
                case 8:
                    for (int j = 0; j < 7; j++) {
                        if (j == 1 || j == 2 || j == 3) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 4:
                    for (int j = 0; j < 8; j++) {
                        if (j == 0 || j == 3) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 9:
                    for (int j = 0; j < 7; j++) {
                        if (j == 1 || j == 2 || j == 3 || j == 4) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
            }
        }
    }

    public static void displayLineFive(int number) {
        String stringNumber = Integer.toString(number);
        for (int i = 0; i < stringNumber.length(); i++) {
            switch (Character.getNumericValue(stringNumber.charAt(i))) {
                case 0:
                    for (int j = 0; j < 9; j++) {
                        if (j == 0 || j == 6) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 1:
                    for (int j = 0; j < 5; j++) {
                        if (j == 1) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 2:
                case 7:
                    for (int j = 0; j < 7; j++) {
                        if (j == 1) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 3:
                case 5:
                case 9:
                    for (int j = 0; j < 7; j++) {
                        if (j == 4) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 4:
                    for (int j = 0; j < 8; j++) {
                        if (j <= 5) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 6:
                case 8:
                    for (int j = 0; j < 7; j++) {
                        if (j == 0 || j == 4) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
            }
        }
    }

    public static void displayLineSix(int number) {
        String stringNumber = Integer.toString(number);
        for (int i = 0; i < stringNumber.length(); i++) {
            switch (Character.getNumericValue(stringNumber.charAt(i))) {
                case 0:
                    for (int j = 0; j < 9; j++) {
                        if (j == 1 || j == 5) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 1:
                    for (int j = 0; j < 5; j++) {
                        if (j == 1) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 2:
                case 7:
                    for (int j = 0; j < 7; j++) {
                        if (j == 0) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 3:
                case 5:
                case 9:
                    for (int j = 0; j < 7; j++) {
                        if (j == 4) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 4:
                    for (int j = 0; j < 8; j++) {
                        if (j == 3) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 6:
                case 8:
                    for (int j = 0; j < 7; j++) {
                        if (j == 0 || j == 4) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
            }
        }
    }

    public static void displayLineSeven(int number) {
        String stringNumber = Integer.toString(number);
        for (int i = 0; i < stringNumber.length(); i++) {
            switch (Character.getNumericValue(stringNumber.charAt(i))) {
                case 0:
                    for (int j = 0; j < 9; j++) {
                        if (j == 2 || j == 3 || j == 4) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 1:
                    for (int j = 0; j < 5; j++) {
                        if (j == 0 || j == 1 || j == 2) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 2:
                    for (int j = 0; j < 7; j++) {
                        if (j <= 4) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 3:
                case 8:
                case 6:
                    for (int j = 0; j < 7; j++) {
                        if (j == 1 || j == 2 || j == 3) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 4:
                    for (int j = 0; j < 8; j++) {
                        if (j == 3) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 5:
                    for (int j = 0; j < 7; j++) {
                        if (j <= 3) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 7:
                    for (int j = 0; j < 7; j++) {
                        if (j == 0) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
                case 9:
                    for (int j = 0; j < 7; j++) {
                        if (j == 4) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    break;
            }
        }
    }
}
