/**
 * Разработать программу, которая позволит при известном годовом проценте
 * вычислить сумму вклада в банке через два года,
 * если задана исходная величина вклада. Вывести результат вычисления в консоль.
 *
 */
public class Task07
{
    public static void main(String[] args)
    {
        int depositSumma = 1500;
        int annualPercentage = 3;
        int yearSum = depositSumma*annualPercentage/100;
        int twoYearsSum = yearSum*2+depositSumma;
        System.out.println("The amount of the deposit in the bank after two years = " + twoYearsSum);
    }
}
