/**
 * В переменной n хранится натуральное (целое) трехзначное число. Создайте
 * программу, вычисляющую и выводящую на экран сумму цифр числа n при любых
 * значения n.
 */
public class Task02
{
    public static void main(String[] args)
    {
        int n = 627;
        int result = 0;
        //Первый способ решения
        String number = Integer.toString(n);
        int j = number.length();
        for (int i=0;i<j;i++) {
        result += Character.getNumericValue(number.charAt(i));
        }

        System.out.println("Sum of digits of the entered number n = " + result);
        System.out.println("Sum of digits of the entered number n = " + sum(n));
    }
        //Второй способ
    private static int sum(int n)
    {
        int result = 0;
        int tmp;
        for (int i=0;i<2;i++)
        {
            tmp = n%10;
            result +=tmp;
            n/=10;
        }
        result+=n;
        return result;
    }
}
