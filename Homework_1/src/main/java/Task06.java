/**
 * Есть прямоугольник у которого известна ширина width и высота height,
 * найти и вывести на консоль периметр и площадь заданного
 * прямоугольника. Высота и ширина прямоугольника должна задаваться константными
 * переменными в коде программы.
 * Вывод: сначала периметр, через пробел площадь
 *
 */
public class Task06
{
    public static void main(String[] args)
    {
        final int width = 10;
        final int height = 3;
        int P = 2*(width + height);
        int S = width*height;
        System.out.printf("P=%d S=%d",P,S);
    }
}
