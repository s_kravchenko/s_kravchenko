import java.util.Scanner;

/**
 * Зная скорость распространения звука в воздушной среде, можно вычислить
 * расстояние до места удара молнии по времени между вспышкой и раскатом грома.
 * Зная время в секундах между вспышкой и раскатом грома (константа в программе)
 * вычислите расстояния до места удара молнии и выведите его на экран.
 */
public class Task08 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        final float SPEED = 340.29f; // м/с
        System.out.println("Enter the time in seconds between the flash of lightning and the rumble of thunder");
        boolean isInt = scanner.hasNextInt();
        if(isInt)
        {
            int time = scanner.nextInt();
            float s = SPEED*time;
            System.out.printf("Distance s = %f м or s = %.2f км", s,(s/1000));
        }
        else
            System.out.println("Sorry, but this is not a number");
        scanner.close();

    }
}
