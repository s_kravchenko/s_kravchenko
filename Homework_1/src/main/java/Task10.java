/**
 * Написать программу расчета идеального веса к росту. В константах хранятся
 * рост (height) и вес (weight), вывести на консоль сообщение, сколько нужно кг
 * набрать или сбросить (идеальный вес = рост - 110).
 * Вывод: положительное число если надо набрать вес, отрицательное если похудеть и ноль если вес идеальный
 */
public class Task10
{
    public static void main(String[] args)
    {
        int height = 198;
        int weight = 95;
        int idealWeight = height - 110;
        if(weight<idealWeight)
            System.out.println(idealWeight-weight);
        else if (weight>idealWeight)
            System.out.println(idealWeight-weight);
        else
            System.out.println(0);
    }
}
