/**
 * Подсчитать площадь и длину окружности для круга с радиусом R. Радиус задан
 * переменной с именем radius. Вывести результат на консоль.
 * Вывод: в одной строке через пробел, сначала окружность, потом площадь
 */
public class Task05
{
    public static void main(String[] args)
    {
        int radius = 10;
        double S = Math.PI * Math.pow(radius,2);
        double C = 2 * Math.PI * radius;
        System.out.printf("C=%f S=%f",C,S);
    }
}
