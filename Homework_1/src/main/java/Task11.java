/**
 * Шаблон для решения домашнеего задания
 * У Деда Мороза есть часы, которые в секундах показывают сколько осталось до
 * каждого Нового Года. Так как Дед Мороз уже человек в возрасте, то некоторые
 * математические операции он быстро выполнять не в состоянии. Помогите Деду
 * Морозу определить сколько полных дней, часов, минут и секунд осталось до
 * следующего Нового Года, если известно сколько осталось секунд, т. е.
 * Разложите время в секундах на полное количество дней, часов, минут и секунд.
 * Выведите результат на консоль.
 */
public class Task11
{
    public static void main(String[] args)
    {
        int SECONDS_TO_NEW_YEAR = 61;
        int day = SECONDS_TO_NEW_YEAR/86400;
        if (day!=0)
            SECONDS_TO_NEW_YEAR%=86400;
        int hour = SECONDS_TO_NEW_YEAR/3600;
        if (hour!=0)
            SECONDS_TO_NEW_YEAR%=3600;
        int minutes = SECONDS_TO_NEW_YEAR/60;
        if (minutes!=0)
            SECONDS_TO_NEW_YEAR%=60;
        System.out.printf("%d Days %d hours %d minutes %d seconds to go until the New Year",day,hour,minutes,SECONDS_TO_NEW_YEAR);
    }
}
