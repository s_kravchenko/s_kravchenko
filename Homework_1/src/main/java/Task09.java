/**
 * Проверить, имеет ли число в переменной типа float вещественную часть.
 * Например, числа 3.14 и 2.5 – имеют вещественную часть, а числа 5.0 и 10.0 – нет.
 */
public class Task09
{
    public static void main(String[] args)
    {
        final float testFloat = 3.14f;
        int testInt =(int)testFloat;
        float floatNumber = testFloat-testInt;
        if (floatNumber == 0)
            System.out.println("The number does not have a real part");
        else
            System.out.println("The number has a real part");
    }
}
