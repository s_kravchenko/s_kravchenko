import java.util.Scanner;

/**
 * Ученикам первого класса дают 1 пирожок, если вес первоклассника менее 30 кг
 * дополнительно дают 1 стакан молока и ещё 1 пирожок. В первых классах школы
 * учится n учеников. Стакан молока имеет ёмкость 200 мл, а упаковка молока –
 * 0,9 л. Написать программу которая определит количество пакетов молока и
 * пирожков, необходимых каждый день для условий: a) Если в школе 100% всех
 * учеников, у которых вес меньше 30 кг. b) Если в школе 60% учеников, имеют вес
 * меньше 30 кг. c) Если в школе 1% учеников, имеют вес меньше 30 кг. (!!!) Учесть,
 * что нельзя купить два с половиной пакета молока; можно купить два или три
 *
 */
public class Task12
{
    public static void main(String[] args)
    {
        int milk;
        int pie;
        int numberOfSchoolBoy;
        int percentOfSchoolBoy;
        int inputPercentOfSchoolBoy;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of students");
        boolean isInt = scanner.hasNextInt();
        if(isInt)
        {
            numberOfSchoolBoy = scanner.nextInt();
            System.out.println("Enter the percentage of students whose weight is less than 30 kg: 100,60,1");
            boolean isInt2 = scanner.hasNextInt();
            if(isInt2)
            {
                inputPercentOfSchoolBoy = scanner.nextInt();
                if(inputPercentOfSchoolBoy!=100&&inputPercentOfSchoolBoy!=60&&inputPercentOfSchoolBoy!=1) {
                    System.out.println("Sorry, Wrong Input");
                }
                else{
                    if (inputPercentOfSchoolBoy == 100) {
                        pie = 2 * numberOfSchoolBoy;
                        milk = (int) Math.ceil((numberOfSchoolBoy * 0.2) / 0.9);
                        System.out.printf("You need %d pies and %d packs of milk", pie, milk);
                    } else if (inputPercentOfSchoolBoy == 60) {
                        percentOfSchoolBoy = (int) (numberOfSchoolBoy * 0.6);
                        pie = numberOfSchoolBoy + percentOfSchoolBoy;
                        milk = (int) Math.ceil((percentOfSchoolBoy * 0.2) / 0.9);
                        System.out.printf("You need %d pies and %d packs of milk", pie, milk);
                    } else {
                        percentOfSchoolBoy = (int) (numberOfSchoolBoy * 0.01);
                        pie = numberOfSchoolBoy + percentOfSchoolBoy;
                        milk = (int) Math.ceil((percentOfSchoolBoy * 0.2) / 0.9);
                        System.out.printf("You need %d pies and %d packs of milk", pie, milk);
                    }
                }
            }
            else
                System.out.println("Sorry, but this is not a number");
        }
        else
            System.out.println("Sorry, but this is not a number");
        scanner.close();
    }
}
