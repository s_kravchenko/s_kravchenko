/**
 * В переменной n хранится вещественное число с ненулевой дробной частью.
 * Создайте программу, округляющую число n до ближайшего целого и выводящую
 * результат округления на экран.
 *
 */
public class Task03
{
    public static void main(String[] args)
    {
        float n = 3.2f;
        System.out.println("The result of rounding the number n = " + Math.round(n));
    }
}
