 import java.util.Scanner;

    /** Написать программу, которая предлагает пользователю
        выбрать животное из списка (1 – кошка, 2 – собака и т.д.), и в
        ответ показывает, какие звуки издает выбранное животное.
        В списке должно быть не менее 10 животных.
    */

public class Task03 {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Choose an animal:");
            System.out.println("1 - Cat");
            System.out.println("2 - Dog");
            System.out.println("3 - Rooster");
            System.out.println("4 - Cow");
            System.out.println("5 - Pig");
            System.out.println("6 - Horse");
            System.out.println("7 - Chicken");
            System.out.println("8 - Neddy");
            System.out.println("9 - Duck");
            System.out.println("10 - Goat");
            boolean isInt = scanner.hasNextInt();
            if(isInt){
                int number = scanner.nextInt();
                switch (number){
                    case 1:
                        System.out.println("Мяу");
                        break;
                    case 2:
                        System.out.println("Гав-гав");
                        break;
                    case 3:
                        System.out.println("Ку-ка-ре-ку");
                        break;
                    case 4:
                        System.out.println("Муууууу");
                        break;
                    case 5:
                        System.out.println("Хрю-хрю");
                        break;
                    case 6:
                        System.out.println("И-го-го");
                        break;
                    case 7:
                        System.out.println("Ко-ко-ко-ко");
                        break;
                    case 8:
                        System.out.println("Иа-а-а");
                        break;
                    case 9:
                        System.out.println("Кря-кря");
                        break;
                    case 10:
                        System.out.println("Беее");
                        break;
                        default:
                            System.out.println("No animal with this number");
                }
            }
            else{
                System.out.println("Invalid value entered");
            }
            scanner.close();

        }
}
