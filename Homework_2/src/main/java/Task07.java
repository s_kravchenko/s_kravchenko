import java.util.Scanner;

/**     Пользователь вводит с клавиатуры букву. Программа
        должна определить, в какой раскладке введена буква – в
        латинской или кирилице. Вывести в консоль: «латиница», если
        буква введена латиницей или «кирилица», если буква
        относится к кирилическому алфавиту. Если введена цифра, а не
        буква, вывести «цифра». Если символ не относится ни к бук-
        вам, ни к цифрам, вывести «невозможно определить»
 */

public class Task07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter character");
        int characterCode = scanner.next().codePointAt(0);
        if(characterCode >= 48 && characterCode <= 57){
            System.out.println("Number");
        }
        else if(characterCode >= 65 && characterCode <= 90 || characterCode >= 97 && characterCode <= 122){
            System.out.println("Latin");
        }
        else if (characterCode >= 1040 && characterCode <= 1103){
            System.out.println("Cyrillic");
        }
        else {
            System.out.println("Unable to determine");
        }
        scanner.close();
    }
}
