import java.util.Scanner;

/**     Даны два числа x и y. Программа должна вывести в консоль
        YES, – если оба числа четные, либо оба числа нечетные;
        иначе программа ничего не выводит
 */

public class Task08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter x and y");
        boolean isInt = scanner.hasNextInt();
        if(isInt){
            int x = scanner.nextInt();
            isInt = scanner.hasNextInt();
            if (isInt) {
                int y = scanner.nextInt();
                if(x%2 == 0 && y%2 == 0 || x%2 !=0 && y%2 != 0){
                    System.out.println("YES");
                }
            }
            else {
                System.out.println("Invalid value entered");
            }
        }
        else{
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
