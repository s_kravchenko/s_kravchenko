import java.util.Scanner;

/**     Организовать ввод с клавиатуры даты рождения человека.
        Программа должна вывести знак зодиака и название года
        по китайскому календарю.  Пример входных данных:
        5 12 1974
        Вывод:
        Знак: Стрелец
        Год: Тигра
 */

public class Task05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the date, month and year of birth, separated by a space");
        boolean isInt = scanner.hasNextInt();
        if(isInt) {
            int day = scanner.nextInt();
            isInt = scanner.hasNextInt();
            if (isInt) {
                int month = scanner.nextInt();
                isInt = scanner.hasNextInt();
                if(isInt) {
                    int year = scanner.nextInt();
                    if(day >= 1 && day <= 31 && month >= 1 && month <= 12) {
                        switch (month) {
                            case 1:
                                if (day < 21) System.out.println("Знак: Козерог");
                                else System.out.println("Знак: Водолей");
                                break;
                            case 2:
                                if (day < 19) System.out.println("Знак: Водолей");
                                else System.out.println("Знак: Рыба");
                                break;
                            case 3:
                                if (day < 21) System.out.println("Знак: Рыба");
                                else System.out.println("Знак: Овен");
                                break;
                            case 4:
                                if (day < 21) System.out.println("Знак: Овен");
                                else System.out.println("Знак: Телец");
                                break;
                            case 5:
                                if (day < 21) System.out.println("Знак: Телец");
                                else System.out.println("Знак: Близнецы");
                                break;
                            case 6:
                                if (day < 22) System.out.println("Знак: Близнецы");
                                else System.out.println("Знак: Рак");
                                break;
                            case 7:
                                if (day < 23) System.out.println("Знак: Рак");
                                else System.out.println("Знак: Лев");
                                break;
                            case 8:
                                if (day < 24) System.out.println("Знак: Лев");
                                else System.out.println("Знак: Дева");
                                break;
                            case 9:
                                if (day < 24) System.out.println("Знак: Дева");
                                else System.out.println("Знак: Весы");
                                break;
                            case 10:
                                if (day < 24) System.out.println("Знак: Весы");
                                else System.out.println("Знак: Скорпион");
                                break;
                            case 11:
                                if (day < 23) System.out.println("Знак: Скорпион");
                                else System.out.println("Знак: Стрелец");
                                break;
                            case 12:
                                if (day < 22) System.out.println("Знак: Стрелец");
                                else System.out.println("Знак: Козерог");
                                break;
                        }
                        switch (year % 12) {
                            case 0:
                                System.out.println("Год: Обезьяны");
                                break;
                            case 1:
                                System.out.println("Год: Петуха");
                                break;
                            case 2:
                                System.out.println("Год: Собаки");
                                break;
                            case 3:
                                System.out.println("Год: Свиньи");
                                break;
                            case 4:
                                System.out.println("Год: Крысы");
                                break;
                            case 5:
                                System.out.println("Год: Быка");
                                break;
                            case 6:
                                System.out.println("Год: Тигра");
                                break;
                            case 7:
                                System.out.println("Год: Кролика");
                                break;
                            case 8:
                                System.out.println("Год: Дракона");
                                break;
                            case 9:
                                System.out.println("Год: Змеи");
                                break;
                            case 10:
                                System.out.println("Год: Лошади");
                                break;
                            case 11:
                                System.out.println("Год: Козы");
                                break;
                        }
                    }
                    else{
                        System.out.println("Ошибка записи числа или месяца");
                    }
                }
                else {
                    System.out.println("Invalid value entered");
                }
            }
            else {
                System.out.println("Invalid value entered");
            }
        }
        else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
