import java.util.Scanner;

/**     Программа запрашивает шестизначное число. После ввода
        определяет, будет ли являться «счастливым» билет с таким
        номером (сумма первых трех цифр совпадает с суммой
        трех последних).
        Пример входных данных:
            423027
            954832
            Вывод:
            Да
            Нет
 */

public class Task12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter enter six-digit number");
        boolean isInt = scanner.hasNextInt();
        int firstPartSum = 0;
        int secondPartSum = 0;
        int tmp;
        if(isInt){
            int number = scanner.nextInt();
            String stringNumber = Integer.toString(number);
            int numberLength = stringNumber.length();
            if(numberLength == 6) {
                for (int i = 0; i < 3; i++) {
                    tmp = number % 10;
                    secondPartSum += tmp;
                    number /= 10;
                }
                for (int i = 0; i < 3; i++) {
                    tmp = number % 10;
                    firstPartSum += tmp;
                    number /= 10;
                }
                if (firstPartSum == secondPartSum) {
                    System.out.println("YES");
                } else {
                    System.out.println("NO");
                }
            }
            else{
                System.out.println("Not a six-digit number entered");
            }
        }
        else{
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
