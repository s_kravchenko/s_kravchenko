import java.util.Scanner;

/**     Найти корни квадратного уравнения и вывести их на
        экран, если они есть. Если корней нет, то вывести сообщение
        об этом. Конкретное квадратное уравнение определяется
        коэффициентами a, b, c, которые вводит пользователь с
        клавиатуры.
 */

public class Task11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("The program solves the following quadratic equation:");
        System.out.println("ax^2 + bx + c = 0");
        System.out.println("Enter the coefficients a, b, c separated by a space");
        boolean isInt = scanner.hasNextInt();
        double D;
        if(isInt) {
            double a = scanner.nextDouble();
            isInt = scanner.hasNextInt();
            if (isInt) {
                double b = scanner.nextDouble();
                isInt = scanner.hasNextInt();
                if(isInt) {
                    double c = scanner.nextDouble();
                    D = b * b - 4 * a * c;
                    if(D > 0){
                        double x1, x2;
                        x1 = (-b - Math.sqrt(D))/ (2 * a);
                        x2 = (-b + Math.sqrt(D))/ (2 * a);
                        System.out.printf("Roots of the equation: x1 = %.3f, x2 = %.3f ",x1,x2);
                    }
                    else if(D == 0){
                        double x;
                        x = -b / (2 * a);
                        System.out.println("The equation has a single root: x = " + x);
                    }
                    else {
                        System.out.println("The equation has no real roots!");
                    }
                }
                else {
                    System.out.println("Invalid value entered");
                }
            }
            else {
                System.out.println("Invalid value entered");
            }
        }
        else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
