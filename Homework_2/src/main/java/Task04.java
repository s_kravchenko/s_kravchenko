import java.util.Scanner;

/**     Дана точка на плоскости заданная координатами x и y,
        определить и вывести в консоль, в какой четверти находит-
        ся точка, в прямоугольной (декартовой) системе координат.
        ( II | I
         III | IV)
 */

public class Task04 {
    public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the coordinates x and y: ");
        boolean firstPoint = scanner.hasNextInt();
        boolean secondPoint = scanner.hasNextInt();
        if(firstPoint && secondPoint){
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            if (x > 0 && y > 0){
                System.out.println("Point is in the 1st quarter");
            }
            else if (x < 0 && y > 0 ){
                System.out.println("Point is in the 2nd quarter");
            }
            else if (x < 0 && y < 0){
                System.out.println("Point is in the 3rd quarter");
            }
            else if (x > 0 && y < 0){
                System.out.println("Point is in the 4th quarter");
            }
            else if (x == 0 && y > 0){
                System.out.println("The point is between the 1st and 2nd quarters");
            }
            else if (x < 0 && y == 0){
                System.out.println("The point is between the 2nd and 3rd quarters");
            }
            else if (x == 0 && y < 0){
                System.out.println("The point is between the 3rd and 4th quarters");
            }
            else if (x > 0 && y == 0){
                System.out.println("The point is between the first and fourth quarters");
            }
            else {
                System.out.println("The point is at the center of the coordinate system");
            }
        }
        else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
