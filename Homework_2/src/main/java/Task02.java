import java.util.Scanner;

/**      С клавиатуры вводится время (количество часов от 0 до
        24) – программа выводит приветствие, соответствующее
        введенному времени (например, ввели 15 часов – выводится
        приветствие «Добрый день»).
*/

public class Task02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the time (from 0 to 24): ");
        boolean isInt = scanner.hasNextInt();
        if(isInt) {
            int time = scanner.nextInt();
            if (time >= 0 && time <= 24){
                if (time >= 5 && time <11){
                    System.out.println("Good morning");
                }
                else if (time >= 11 && time <17){
                    System.out.println("Good afternoon");
                }
                else if (time >= 17 && time < 23){
                    System.out.println("Good evening");
                }
                else {
                        System.out.println("Goodnight");
                    }
                }
            else {
                System.out.println("Invalid value entered");
            }
        }
        else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
