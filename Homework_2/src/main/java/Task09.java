import java.util.Scanner;

/**     Даны координаты начала и координаты конца отрезка.
        Если считать отрезок обозначением горки, то в одном случае
        он обозначает спуск, в другом – подъем. Определить и
        вывести на экран – спуск это или подъем, ровная дорога или
        вообще отвесная.
 */

public class Task09 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter x1 and y1");
        boolean isInt = scanner.hasNextInt();
        if(isInt){
            int x1 = scanner.nextInt();
            isInt = scanner.hasNextInt();
            if (isInt) {
                int y1 = scanner.nextInt();
                System.out.println("Enter x2 and y2");
                isInt = scanner.hasNextInt();
                if(isInt){
                    int x2 = scanner.nextInt();
                    isInt = scanner.hasNextInt();
                    if(isInt){
                        int y2 = scanner.nextInt();
                        if(x2 > x1 && y1 > y2 || x1 > x2 && y1 > y2){
                            System.out.println("Descent");
                        }
                        else if (x2 > x1 && y2 > y1 || x1 > x2 && y2 > y1){
                            System.out.println("Rise");
                        }
                        else if (y1 == y2){
                            System.out.println("The road is level");
                        }
                        else {
                            System.out.println("Steep road");
                        }
                    }
                    else {
                        System.out.println("Invalid value entered");
                    }
                }
                else {
                    System.out.println("Invalid value entered");
                }
            }
            else {
                System.out.println("Invalid value entered");
            }
        }
        else{
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
