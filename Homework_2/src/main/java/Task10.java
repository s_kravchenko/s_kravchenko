import java.util.Scanner;

/**     Определить номер подъезда девятиэтажного дома, по
        указанному номеру квартиры N. Считать, что на каждом
        этаже находится M квартир. Вывести в консоль номер подъезда.
*/

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the apartment number");
        boolean isInt = scanner.hasNextInt();
        if(isInt){
            int apartmentNumber = scanner.nextInt();
            System.out.println("Enter the number of apartments per floor");
            isInt = scanner.hasNextInt();
            if (isInt) {
                int numberOfApartmentsPerFloor = scanner.nextInt();
                int numberOfApartmentsInTheEntrance = 9 * numberOfApartmentsPerFloor;
                int entrance = (int) Math.ceil( (double) apartmentNumber/numberOfApartmentsInTheEntrance);
                System.out.println("Entrance number - " + entrance);
            }
            else {
                System.out.println("Invalid value entered");
            }
        }
        else{
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
