import java.util.Scanner;

/**      Написать программу, которая предлагает пользователю
        ввести c клавиатуры номер дня недели, и в ответ показыва-
        ет название этого дня (например, 6 – это суббота). Решить с
        использованием switch.

 */

public class Task01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of the day of the week");
        boolean isInt = scanner.hasNextInt();
        if(isInt) {
            int day = scanner.nextInt();
            if(day >= 1 && day <= 7) {
                switch (day) {
                    case 1:
                        System.out.println("Monday");
                        break;
                    case 2:
                        System.out.println("Tuesday");
                        break;
                    case 3:
                        System.out.println("Wednesday");
                        break;
                    case 4:
                        System.out.println("Thursday");
                        break;
                    case 5:
                        System.out.println("Friday");
                        break;
                    case 6:
                        System.out.println("Saturday");
                        break;
                    case 7:
                        System.out.println("Sunday");
                        break;
                }
            }
            else {
                System.out.println("Invalid value entered");
            }
        }
        else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
