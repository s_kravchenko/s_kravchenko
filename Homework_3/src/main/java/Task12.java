import java.util.Scanner;

/**    Проверить, есть ли в двоичной записи числа n хотя бы
       один 0. Используя только побитовые и условные операторы.
       Вывести на консоль исходное число в двоичном виде и результат.
 */

public class Task12 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        boolean isInt = scanner.hasNextInt();
        if (isInt) {
            int number = scanner.nextInt();
            System.out.println("Введённое число в двоичном виде:");
            System.out.println(Integer.toBinaryString(number));
            boolean isHasZero = false;
            for (int i = 0; i < Integer.toBinaryString(number).length(); i++) {
                if ((number & (1 << i)) == 0) {
                    isHasZero = true;
                    break;
                }
            }
            if (isHasZero) {
                System.out.println("В двочной записи числа есть нули");
            }
            else {
                System.out.println("В двоичной записи числа нулей нет");
            }
        }
        else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
