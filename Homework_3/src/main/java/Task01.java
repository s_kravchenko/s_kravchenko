import java.util.Scanner;

 /**    Обнулить бит в нулевом разряде числа N. Остальные
        биты не должны изменить свое значение. Вывести на кон-
        соль в двоичном виде
*/

public class Task01 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        boolean isInt = scanner.hasNextInt();
        if (isInt){
        int number = scanner.nextInt();
            System.out.println("Введённое число в двоичном виде:");
            System.out.println(Integer.toBinaryString(number));
        //number = number & (-1<<1);
            System.out.print("Результат: " + Integer.toBinaryString(number & (-1 << 1)));
        }
        else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
