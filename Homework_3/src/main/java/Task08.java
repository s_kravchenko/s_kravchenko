import java.util.Scanner;

/**    Обнулить крайний левый (старший разряд) единичный
       бит числа N. Вывести результат на консоль в двоичном виде.
 */

public class Task08 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        boolean isInt = scanner.hasNextInt();
        if (isInt){
            int number = scanner.nextInt();
            System.out.println("Введённое число в двоичном виде:");
            System.out.println(Integer.toBinaryString(number));
            int numberOfSeniorBit = 0;
            int tmp = number;
            while (tmp != 1){
                tmp >>= 1;
                numberOfSeniorBit++;
            }
            System.out.println("Результат: " + Integer.toBinaryString(number & ~ (1 << numberOfSeniorBit)));
        }
        else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
