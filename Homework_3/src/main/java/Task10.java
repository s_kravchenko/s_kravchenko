import java.util.Scanner;

/**    Найти и вывести на консоль минимальное из двух чисел
       M и N, используя только побитовые операции.
 */

public class Task10 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первое число:");
        boolean isInt = scanner.hasNextInt();
        if (isInt){
            int firstNumber = scanner.nextInt();
            System.out.println("Введённое число в двоичном виде:");
            System.out.println(Integer.toBinaryString(firstNumber));
            System.out.println("Введите второе число:");
            isInt = scanner.hasNextInt();
            if (isInt){
                int secondNumber = scanner.nextInt();
                System.out.println("Введённое число в двоичном виде:");
                System.out.println(Integer.toBinaryString(secondNumber));
                int min = firstNumber & ((firstNumber - secondNumber) >> 31) | secondNumber & (~(firstNumber - secondNumber) >> 31);
                System.out.print("Результат: " + Integer.toBinaryString(min));
            }
            else {
                System.out.println("Invalid value entered");
            }
        }
        else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
