import java.util.Scanner;

/**    Установить i-й бит числа N равным 0. Вывести результат
       на консоль в двоичном виде.
 */

public class Task05 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        boolean isInt = scanner.hasNextInt();
        if (isInt){
            int number = scanner.nextInt();
            System.out.println("Введённое число в двоичном виде:");
            System.out.println(Integer.toBinaryString(number));
            System.out.println("Введите бит числа N, который нужно установить в 0");
            isInt = scanner.hasNextInt();
            if (isInt){
                int bit = scanner.nextInt();
                System.out.print("Результат: " + Integer.toBinaryString(number & ~ (1 << (bit-1))));
            }
            else {
                System.out.println("Invalid value entered");
            }
        }
        else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
