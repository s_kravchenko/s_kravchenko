import java.util.Scanner;

/**    Определить имеют ли числа M и N разные знаки.
       Используя только побитовые и условные операторы.
 */

public class Task09 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первое число:");
        boolean isInt = scanner.hasNextInt();
        if (isInt){
            int firstNumber = scanner.nextInt();
            System.out.println("Введённое число в двоичном виде:");
            System.out.println(Integer.toBinaryString(firstNumber));
            System.out.println("Введите второе число:");
            isInt = scanner.hasNextInt();
            if (isInt){
                int secondNumber = scanner.nextInt();
                System.out.println("Введённое число в двоичном виде:");
                System.out.println(Integer.toBinaryString(secondNumber));
                boolean isTru = (firstNumber ^ secondNumber) >= 0;
                if (isTru){
                    System.out.print("Оба числа имеют одинаковый знак");
                }
                else {
                    System.out.println("Числа имеют разные знаки");
                }
            }
            else {
                System.out.println("Invalid value entered");
            }
        }
        else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
