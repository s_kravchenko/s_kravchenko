import java.util.Scanner;

/**    Посчитать и вывести на консоль количество единичных
       битов в числе N. Вывести на консоль исходное число в
       двоичном виде и результат.
 */

public class Task11 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        boolean isInt = scanner.hasNextInt();
        if (isInt){
            int number = scanner.nextInt();
            System.out.println("Введённое число в двоичном виде:");
            System.out.println(Integer.toBinaryString(number));
            int count;
            /*for (int i = 0; i <Integer.toBinaryString(number).length() ; i++) {
                if((number >> i & 1) == 1){
                count++;
                }
            }*/
            for (count = 0; number>0 ; number >>= 1) {
                count += number & 1;
            }
            System.out.println("Колличество единичных битов = " + count);
        }
        else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
