import java.util.Scanner;

/**    Вывести на консоль 2 в степени n. Для вычисления
       использовать только побитовые операции.
 */

public class Task02 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите в какую степень возвести число 2:");
        boolean isInt = scanner.hasNextInt();
        if (isInt){
            int exponent = scanner.nextInt();
            System.out.print("Результат: " + (1 << exponent));
        }
        else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
