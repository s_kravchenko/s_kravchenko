import java.util.Scanner;

/**    Определить значение i-го бита числа N. Вывести резуль-
       тат на консоль в двоичном виде.
 */

public class Task07 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        boolean isInt = scanner.hasNextInt();
        if (isInt){
            int number = scanner.nextInt();
            System.out.println("Введённое число в двоичном виде:");
            System.out.println(Integer.toBinaryString(number));
            System.out.println("Введите номер бита, значение которого нужно найти");
            isInt = scanner.hasNextInt();
            if (isInt){
                int bit = scanner.nextInt();
                System.out.print("Результат: " + Integer.toBinaryString((number >> bit-1) & 1 ));
            }
            else {
                System.out.println("Invalid value entered");
            }
        }
        else {
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
