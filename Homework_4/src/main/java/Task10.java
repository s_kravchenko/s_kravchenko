/**
 *      2520 – наименьшее число, которое можно разделить на
 *      каждое из чисел от 1 до 10, без остатка. Написать программу,
 *      которая рассчитывает наименьшее положительное число,
 *      которое делится на все числа от 1 до 20.
 */

public class Task10 {
    public static void main(String[] args) {
        int number = 1;
        int i = 1;
        while (i <= 20) {
            if (number % i == 0) {
                i++;
            } else {
                number++;
                i = 1;
            }
        }
        System.out.println("Lcm = " + number);
        findMultiple();
    }
    static void findMultiple()
    {
        long lcm = 1;
        for(long i = 2;i <= 20;i++)
        {
            lcm *= i/gcd(lcm,i);
        }
        System.out.println("Lcm = " + lcm);
    }
    static long gcd(long a, long b) {
        while(b > 0) {
            a %= b;
            if (a == 0) return b;
            b %= a;
        }
        return a;
    }
}
