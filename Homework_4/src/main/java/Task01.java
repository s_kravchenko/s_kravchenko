/**
 *      Показать на экране все числа Фибоначчи в диапазоне от
 *      0 до 10 000 000.
 */

public class Task01 {
    public static void main(String[] args) {
        int fibonachiFirst = 0;
        int fibonachiSecond = 1;
        int fibonachi = 0;
        System.out.println("Fibonacci numbers:");
        System.out.print(fibonachiFirst + " " + fibonachiSecond + " ");
        while (fibonachi < 10000000) {
            fibonachi = fibonachiFirst + fibonachiSecond;
            if (fibonachi < 10000000){
                System.out.print(fibonachi + " ");
            }
            fibonachiFirst = fibonachiSecond;
            fibonachiSecond = fibonachi;
        }
    }
}
