/**
 *      Показать на экране все совершенные числа в диапазоне от
 *      0 до 1 000 000.
 */

public class Task04 {
    public static void main(String[] args) {
        int perfect;
        int sum;
        System.out.println("Perfect numbers ranging from 0 to 1 000 000:");
        for (int i = 1; i < 10000; i++) {
            perfect = i;
            sum = 0;
            for (int j = 1; j <= perfect/2; j++) {
                if (perfect % j == 0) {
                    sum += j;
                }
            }
            if (sum == perfect) {
                System.out.println(perfect);
            }
        }
    }
}
