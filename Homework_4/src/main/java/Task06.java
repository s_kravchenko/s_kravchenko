/**
 *      Вывести  на консоль все восьмизначные числа, цифры в
 *      которых не повторяются. Эти числа должны делиться на
 *      12345, без остатка. Показать общее количество найденных
 *      чисел.
 */

public class Task06 {
    public static void main(String[] args) {
        int digit = 12345;
        int number1;
        int number2;
        int count = 0;
        int mod1;
        int mod2;
        first:
        for (int i = 10000000; i < 100000000; i++) {
            number1 = i;
            for (int j = 1; j <= 8; j++) {
                mod1 = number1 % 10;
                number2 = number1 / 10;
                while (number2 > 0) {
                    mod2 = number2 % 10;
                    number2 /= 10;
                    if (mod1 == mod2) {
                        continue first;
                    }
                }
                number1 /= 10;
            }
            if (i % digit == 0) {
                count++;
                System.out.print(i + " ");
            }
        }
        System.out.print('\n' + "Amount: " + count);
    }
}
