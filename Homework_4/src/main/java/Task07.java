import java.util.Scanner;

/**
 * Показать битовое представление значения переменной
 * типа int, используя только один цикл, управляющую пере-
 * менную, вывод на консоль и битовые операции.
 * Не использовать строки и любые другие готовые функции (методы).
 */

public class Task07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number:");
        boolean isInt = scanner.hasNextInt();
        if (isInt) {
            int number = scanner.nextInt();
            for (int i = 32; i > 0; i--) {
                if ((number & (1 << i - 1)) != 0) {
                    System.out.print("1");
                } else {
                    System.out.print("0");
                }
            }
        } else {
            System.out.println("Invalid value entered");
        }

    }
}
