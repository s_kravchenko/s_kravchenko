/**
 *      Напишите программу, которая выводит на экран числа
 *      от 1 до 1 000. При этом вместо чисел, кратных трём,
 *      программа должна выводить слово Fizz, а вместо
 *      чисел, кратных пяти - слово Buzz. Если число кратно
 *      пятнадцати, то программа должна выводить вместо
 *      числа слово  Hiss
 */

public class Task11 {
    public static void main(String[] args) {
        for (int i = 1; i <= 1000; i++) {
            if(i%15 == 0){
                System.out.print("Hiss ");
            }
            else if(i%5 == 0){
                System.out.print("Buzz ");
            }
            else if(i%3 == 0){
                System.out.print("Fizz ");
            }
            else {
                System.out.print(i + " ");
            }
        }
    }
}
