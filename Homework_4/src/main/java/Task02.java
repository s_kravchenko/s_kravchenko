/**
 *      Написать программу, которая выводит на экран все простые
 *      числа в диапазоне от 2 до 1 000 000. Постарайтесь не
 *      выполнять лишних действий (например, после того как вы
 *      нашли хотя бы один нетривиальный делитель, уже ясно, что
 *      число составное и проверку продолжать не нужно). Также
 *      учтите, что наименьший делитель натурального числа n,
 *      если он вообще имеется, обязательно располагается в отрезке [2; √n].
 */

public class Task02 {
    public static void main(String[] args) {
        int count = 0;
        System.out.println("Prime numbers from 2 to 1 000 000:");
        two:
        for (int i = 2; i < 1000000; i++) {
            int rightMax = (int) Math.sqrt(i);
            for (int j = 2; j <= rightMax; j++) {
                if (i % j == 0) {
                    continue two;
                }
            }
            count++;
            System.out.print(i + " ");
            if (count % 20 == 0) {
                System.out.println();
            }
        }
    }
}
