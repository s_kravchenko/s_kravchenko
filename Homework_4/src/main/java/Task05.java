import java.util.Scanner;

/**
 * Напишите программу, которая будет проверять, является
 * ли число, введенное с клавиатуры палиндромом (одинаково
 * читающееся в обоих направлениях). Например, 123454321
 * или 221122 – палиндром. Программа должна вывести YES,
 * если число является палиндромом, и NO – в противополож-
 * ном случае.
 */

public class Task05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number:");
        boolean isInt = scanner.hasNextInt();
        if (isInt) {
            int number = scanner.nextInt();
            int b = 0;
            for (int i = number, j = i % 10; i > 0; i = i / 10, j = i % 10) {
                b = b * 10 + j;
            }
            System.out.println(number == b?"YES":"NO");
        }
        else {
            System.out.println("Invalid value entered");
        }
    }
}
