import java.util.Scanner;

/**
 *      В первый день спортсмен пробежал X километров, а затем
 *      он каждый день увеличивал пробег на 10% от предыдущего
 *      значения. По числу, указанному с клавиатуры Y, определите
 *      номер дня, на который пробег спортсмена составит не менее
 *      Y километров.
 */

public class Task12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of kilometers the athlete ran on the first day:");
        boolean isInt = scanner.hasNextInt();
        if (isInt){
            double distance = scanner.nextInt();
            System.out.println("Enter the number of kilometers that the athlete must run:");
            isInt = scanner.hasNextInt();
            if (isInt){
                int finalDistance = scanner.nextInt();
                int day = 1;
                double sumDistance = distance;
                while (sumDistance <= finalDistance){
                    distance = distance*1.1;
                    sumDistance += distance;
                    day++;
                }
                System.out.print("Number of the day on which the athlete will run ");
                System.out.print("required number of kilometers: " + day);
            }
            else{
                System.out.println("Invalid value entered");
            }
        }
        else{
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
