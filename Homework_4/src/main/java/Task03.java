/**
 *      Показать на экране все числа Армстронга в диапазоне от
 *      10 до 1 000 000.
 */

public class Task03 {
    public static void main(String[] args) {
        int number;
        int armstrong;
        int n;
        int count;
        System.out.println("Armstrong numbers ranging from 10 to 1 000 000:");
        for (int i = 153; i < 1000000; i++) {
            n = i;
            armstrong = 0;
            count = 0;
            while (n != 0){
                n/=10;
                count++;
            }
            n = i;
            while (n != 0){
                number = n%10;
                armstrong += Math.pow(number,count);
                n/=10;
            }
            if(armstrong == i){
                System.out.print(i + " ");
            }
        }
    }
}
