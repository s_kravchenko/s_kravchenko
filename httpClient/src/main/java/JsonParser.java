import org.json.JSONObject;

public class JsonParser {

    public Post getPost(String response){
        org.json.JSONObject postJson = new JSONObject(response);
        int userId = postJson.getInt("userId");
        int articleId = postJson.getInt("id");
        String title = postJson.getString("title");
        String message = postJson.getString("body");
        return new Post(userId,articleId,title,message);
    }

}
