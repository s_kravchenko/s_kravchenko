import com.google.gson.Gson;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpClient {
    private static String url = "https://jsonplaceholder.typicode.com/posts/";

    public static void main(String[] args) throws IOException {
        if (args[0].toUpperCase() == "GET") {
            sendGet(Integer.parseInt(args[1]));
        } else if (args[0].toUpperCase() == "POST") {
            sendPost();
        }
        else {
            System.out.println("Error");
        }
        /*sendGet(1);
        sendPost();*/
    }

    public static void sendGet(int postId) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(url + postId).openConnection();
        connection.setRequestMethod("GET");
        InputStream in;
        int responseCode = connection.getResponseCode();
        if (responseCode != HttpURLConnection.HTTP_OK && responseCode != HttpURLConnection.HTTP_NOT_MODIFIED) {
            in = connection.getErrorStream();
        } else {
            in = connection.getInputStream();
        }

        String response = convertStreamToString(in);
        //Post post = new JsonParser().getPost(response);
        Gson gson = new Gson();
        Post post = gson.fromJson(response, Post.class);
        System.out.println(post);
    }

    public static void sendPost() throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);

        Post newPost = new Post(3, "title", "message");
        //JSONObject object = new JSONObject(newPost);
        Gson g = new Gson();
        String urlParameters = g.toJson(newPost);
        //String urlParameters = object.toString();
        //String urlParameters = "\"userId\":3,\"title\":\"title\",\"body\":\"message\"";

        DataOutputStream out = new DataOutputStream(connection.getOutputStream());
        out.writeBytes(urlParameters);
        out.flush();
        out.close();

        InputStream in;
        int responseCode = connection.getResponseCode();

        if (responseCode != HttpURLConnection.HTTP_CREATED) {
            in = connection.getErrorStream();
        } else {
            in = connection.getInputStream();
        }

        /*String response = convertStreamToString(in);
        //Post post = new JsonParser().getPost(response);
        Gson gson = new Gson();
        Post post = gson.fromJson(response, Post.class);
        System.out.println("Article [" + post.getId() + "] has been created: User [" +
                post.getUserId() + "] Title [" + post.getTitle() + "] Message [" + post.getBody() + "]");*/

        String response = convertStreamToString(in);
        Gson gson = new Gson();
        Post post = gson.fromJson(response, Post.class);
        System.out.println("Article [" + post.getId() + "] has been created: User [" +
                newPost.getUserId() + "] Title [" + newPost.getTitle() + "] Message [" + newPost.getBody() + "]");

    }

    public static String convertStreamToString(InputStream stream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        stream.close();

        return sb.toString();
    }


}
