import java.util.Scanner;

/**
 * Слова в языке Мумба-Юмба могут состоять только из букв
 * a, b, c, и при этом:
 * - никогда не содержат двух букв b подряд;
 * -  ни в одном слове никогда не встречается три одинаковых
 * подслова подряд. Например, по этому правилу, в язык
 * Мумба-Юмба не могут входить слова aaa (так как три
 * раза подряд содержит подслово a), ababab (так как три
 * раза подряд содержит подслово ab), aabcabcabca (три
 * раза подряд содержит подслово abc).
 * Все слова, удовлетворяющие вышеописанным правилам,
 * входят в язык Мумба-Юмба.
 * Напишите программу, которая по данному слову (введе-
 * ного с клавиатуры) определит, принадлежит ли оно этому
 * языку.
 */

public class Task11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите слово, состоящее из букв \"a\",\"b\" и \"c\"");
        String word = scanner.next();

            if (word.matches("[abc]+")) {
                if (word.contains("bb")) {
                    System.out.println("Не является, введены две буквы \"b\" подряд");
                } else if (word.contains("aaa") || word.contains("ccc") || word.contains("ababab") ||
                        word.contains("abcabcabc") || word.contains("bcbcbc") || word.contains("bababa") ||
                        word.contains("cbcbcb") || word.contains("acacac") || word.contains("cacaca") ||
                        word.contains("cbacbacba") || word.contains("cbccbccbc") || word.contains("bacbacbac") ||
                        word.contains("cabcabcab") || word.contains("bcabcabca") || word.contains("acbacbacb") ||
                        word.contains("abaabaaba") || word.contains("aabaabaab") || word.contains("baabaabaa") ||
                        word.contains("accaccacc") || word.contains("ccaccacca") || word.contains("acaacaaca") ||
                        word.contains("caccaccac")) {
                    System.out.println("Не является, так как встречается три одинаковых подслова подряд");
                } else {
                    System.out.println("Слово входит в язык Мумба-Юмба");
                }
            } else {
                System.out.println("Не является, введены недозволенные символы");
            }
        scanner.close();
    }
}
