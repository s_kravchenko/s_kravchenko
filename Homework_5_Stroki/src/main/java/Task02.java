import java.util.Scanner;

/**
 *      Написать программу, которая создаст строку, в которой
 *      находятся все целые числа, начиная с 1, выписаны в одну
 *      строку «123456789101112131415...». Строка должна быть
 *      длиной не более 1 000 символов.
 *      По числу n (введенного с клавиатуры), выведите цифру на
 *      n-й позиции (используется нумерация с 1).
 */

public class Task02 {
    public static void main(String[] args) {
        String line = "";
        for (int i = 1; i <= 1000 ; i++) {
            line += i;
        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите номер n-й позиции, значение которой нужно вывести");
        boolean isInt = scanner.hasNextInt();
        if (isInt) {
            int number = scanner.nextInt();
            if (number <= line.length()) {
                System.out.println(line);
                System.out.print(line.charAt(number - 1));
            }
            else {
                System.out.println("Превышен максимальный номер позиции");
            }
        }
        else{
            System.out.println("Invalid value entered");
        }
        scanner.close();
    }
}
