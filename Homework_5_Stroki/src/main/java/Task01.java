import java.util.Scanner;

/**
 * Ввести с клавиатуры строку текста, а затем один символ.
 * Показать на консоль индексы и количество совпадений (ищем
 * вхождения символа в строку). В случае если совпадений не
 * найдено, вывести соответствующий текст.
 */

public class Task01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку текста:");
        String line = scanner.nextLine();
        System.out.println("Введите символ:");
        String symbol = scanner.nextLine();
        int count = 0;

        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == symbol.charAt(0)) {
                count++;
                System.out.print("Индексы совпадения " + (i) + " ");
            }
        }
        if (count == 0) {
            System.out.println("Совпадений не найдено!");
        } else {
            System.out.println("\n" + "Количество совпадений: " + count);
        }
        scanner.close();
    }
}
