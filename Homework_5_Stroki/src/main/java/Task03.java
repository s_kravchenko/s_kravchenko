import java.util.Scanner;

/**
 *      Подсчитать среднюю длину слова, во введенном с
 *      клавиатуры предложении.
 */

public class Task03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите предложение:");
        String line = scanner.nextLine();
        String[] masLine = line.split("\\s+");
        int numberOfWords = masLine.length;
        String lineWithoutSpace = line.replaceAll("\\s+", "");
        char[] masOfSymbols = lineWithoutSpace.toCharArray();
        int numberOfSymbols = masOfSymbols.length;
        System.out.println(Math.round((double) numberOfSymbols / numberOfWords));
        scanner.close();
    }
}
