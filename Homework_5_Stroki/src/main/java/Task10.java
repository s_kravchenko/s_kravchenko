import java.util.Scanner;

/**
 * Напишите программу, которая будет печатать
 * ромбовидный рисунок на основе строки, введенной с
 * клавиатуры (максимальная длина – 50 символов).
 * Пример вывода для строки testing:
 *       t
 *      te
 *     tes
 *    test
 *   testi
 *  testin
 * testing
 * esting
 * sting
 * ting
 * ing
 * ng
 * g
 */

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        String line = scanner.nextLine();
        char[] lineCharArray;
        String newLine = "";

        if (line.length() < 50) {
            lineCharArray = line.toCharArray();
        } else {
            lineCharArray = line.substring(0, 50).toCharArray();
            System.out.println(String.valueOf(lineCharArray));
        }
        for (char c : lineCharArray) {
            newLine += String.valueOf(c);
            System.out.printf("%" + lineCharArray.length + "s%n", newLine);
        }

        String finalLine = newLine;

        for (int i = 0; i < newLine.length(); i++) {
            finalLine = finalLine.replaceFirst("[^.]", "");
            System.out.println(finalLine);
        }
        scanner.close();
    }
}
