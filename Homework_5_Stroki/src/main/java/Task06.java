/**
 *      В американской армии считается несчастливым число 13,
 *      а в китайской – 4. Перед совместными учениями с аме-
 *      риканской и китайской армией, штаб украинской армии
 *      решил исключить номера боевой техники, содержащие
 *      числа 4 или 13 (например, 4 0123, 13 373, 123 4 5 или 6 134 2),
 *      чтобы не смущать иностранных коллег.
 *      Написать программу, которая выведет на экран сколько
 *      всего номеров придется исключить, если в распоряжении
 *      армии имеется 100 тысяч единиц боевой техники и каждая
 *      боевая машина имеет номер от 00000 до 99999. Решить,
 *      используя строки.
 */

public class Task06 {
    public static void main(String[] args) {
        String line = "";
        int count = 0;

        for (int i = 0; i < 100000 ; i++) {
            if (i<10){
                line += "0000" + i + " ";
            }
            else if (i>=10 && i<100){
                line += "000" + i + " ";
            }
            else if (i>=100 && i<1000){
                line += "00" + i + " ";
            }
            else if (i>=1000 && i<10000){
                line += "0" + i + " ";
            }
            else {
                line += i + " ";
            }
        }
        String[] masStringNumbers = line.split("\\s+");
        String[] masStringNumbersAfterReplace = new String[masStringNumbers.length];

        for (int i = 0; i < masStringNumbers.length; i++) {
            String word = masStringNumbers[i].replaceAll(".*13.*|.*4.*",".");
            masStringNumbersAfterReplace[i] = word;
            if (!(masStringNumbers[i].equals(masStringNumbersAfterReplace[i]))){
                count++;
            }
        }

        System.out.println("Количество номеров, которые нужно исключить: " + count);
    }
}
