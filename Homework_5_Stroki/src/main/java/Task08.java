import java.util.Scanner;

/**
 * В языке Java принято первое слово, входящее в название
 * переменной, записывать с маленькой латинской буквы.
 * Следующее слово идет с большой буквы (только первая
 * буква слова – большая). Слова не имеют разделителей и
 * состоят только из латинских букв. Например, правильные
 * записи переменных в Java могут выглядеть следующим
 * образом:  javaIdentifier,  longAndMnemonicIdentifier,
 * name, nEERC.
 * В языке C++ для описания переменных используются
 * только маленькие латинские символы и символ «_»,
 * который отделяет непустые слова друг от друга. Если
 * строка имеет смешанный синтаксис: например java_
 * Identifier,  сообщить об этом. Примеры: java_identifier,
 * long_and_mnemonic_identifier, name, n_e_e_r_c.
 * <p>
 * Вам требуется написать программу, которая преобразует
 * переменную, записанную на одном языке, в формат другого
 * языка. Идентификатор (имя) переменной должен вводится с
 * клавиатуры. Программа должна определить, из какого языка
 * взята переменная, и переделать ее в переменную другого
 * языка. Вывести результат на консоль.
 */

public class Task08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите имя переменной:");
        String variable = scanner.next();
        char[] masVariable = variable.toCharArray();
        String newVariable = "";

        if (variable.matches("^[a-z][a-zA-Z0-9]*$")) {
            System.out.println("Это JAVA");
            for (int i = 0; i < masVariable.length; i++) {
                if (Character.isUpperCase(Character.codePointAt(masVariable, i))) {
                    masVariable[i] = (char) Character.toLowerCase(Character.codePointAt(masVariable, i));
                    newVariable += "_" + masVariable[i];
                } else {
                    newVariable += masVariable[i];
                }
            }
            System.out.println(newVariable);
        } else if (variable.matches("^[a-z][a-z_]*[a-z0-9]+$")) {
            System.out.println("Это C++");
            for (int i = 0; i < masVariable.length; i++) {
                if (masVariable[i] == '_') {
                    masVariable[i + 1] = (char) Character.toUpperCase(Character.codePointAt(masVariable, i + 1));
                    newVariable += masVariable[i];
                } else {
                    newVariable += masVariable[i];
                }
            }
            newVariable = newVariable.replaceAll("_", "");
            System.out.println(newVariable);
        } else if (variable.matches("^[a-z]\\w+[^_]$")) {
            System.out.println("Смешанный синтаксис");
        } else {
            System.out.println("Неверный синтаксис");
        }
        scanner.close();
    }
}
