import java.util.Scanner;

/**
 * Пользователь вводит с клавиатуры любую строку.
 * Поменять в исходной строке все большие буквы на
 * маленькие, а маленькие – на большие. Если в строке при-
 * сутствуют цифры, заменить на символ подчеркивания и
 * вывести результат на консоль.
 */

public class Task07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        String line = scanner.nextLine();
        char[] charsLine = line.toCharArray();

        for (int i = 0; i < charsLine.length; i++) {
            if (Character.isLowerCase(Character.codePointAt(charsLine, i))) {
                charsLine[i] = (char) Character.toUpperCase(Character.codePointAt(charsLine, i));
            } else if (Character.isUpperCase(Character.codePointAt(charsLine, i))) {
                charsLine[i] = (char) Character.toLowerCase(Character.codePointAt(charsLine, i));
            }
            if (Character.isDigit(Character.codePointAt(charsLine, i))) {
                charsLine[i] = '_';
            }
            System.out.print(charsLine[i]);
        }
        scanner.close();
    }
}
