import java.util.Scanner;

/**
 * Ввести строку с клавиатуры. В строке должны содержаться
 * слова, которые могут быть раздельные пробелами или
 * двоеточиями. Необходимо вычислить количество слов в
 * строке, у которых четное количество букв.
 */

public class Task05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите предложение:");
        String line = scanner.nextLine();
        String[] masLine = line.split("[\\s:]+");
        int count = 0;
        for (String word : masLine) {
            if (word.length() % 2 == 0) {
                count++;
            }
        }
        System.out.println("Количество слов в строке, у которых четное количество букв: " + count);
        scanner.close();
    }
}
