import java.util.Arrays;
import java.util.Scanner;

/**
 * Написать программу, проверяющую является ли одна
 * строка анаграммой для другой строки (строка может состоять
 * из нескольких слов и символов пунктуации). Пробелы и
 * пунктуация должны игнорироваться при анализе. Разница
 * в больших и маленьких буквах должна игнорироваться. Обе
 * строки должны вводиться с клавиатуры. Программа должна
 * выводить  Yes, если строки являются анаграммой, и No –
 * иначе.
 * Пример анаграммы в стихах:
 * Строка 1 «Аз есмь строка, живу я, мерой остр».
 * Строка 2 «За семь морей ростка я вижу рост!»
 */

public class Task09 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строки:");
        String lineOne = scanner.nextLine();
        String lineTwo = scanner.nextLine();
        String lineOneFinal = lineOne.replaceAll("\\W", "").toLowerCase();
        String lineTwoFinal = lineTwo.replaceAll("\\W", "").toLowerCase();
        char[] lineOneCharArray = lineOneFinal.toCharArray();
        char[] lineTwoCharArray = lineTwoFinal.toCharArray();
        Arrays.sort(lineOneCharArray);
        Arrays.sort(lineTwoCharArray);
        if (lineOneFinal.length() != lineTwoFinal.length()) {
            System.out.println("No");
        } else if (Arrays.equals(lineOneCharArray, lineTwoCharArray)) {
            System.out.println("Yes");
        }
        scanner.close();
    }
}
