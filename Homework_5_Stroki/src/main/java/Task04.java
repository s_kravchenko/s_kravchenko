import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Ввести строку с клавиатуры (латиницей). Из введенной
 * строки выбрать все слова, начинающиеся на гласные буквы
 * и заканчивающиеся на согласные. Вывести отобранные
 * слова на консоль.
 */

public class Task04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите предложение:");
        String line = scanner.nextLine();
        String[] masLine = line.split("\\s+");
        Pattern pattern = Pattern.compile("^[aeyuioAEYUIO][a-zA-Z]+[^aeyuioAEYUIO]$");
        for (String word : masLine) {
            Matcher matcher = pattern.matcher(word);
            if (matcher.matches()) {
                System.out.println(word);
            }
        }
        scanner.close();
    }
}
